extends SceneTree

const TEXT = 'N’est-il pas diablement aisé de se prendre pour un grand homme quand on ne soupçonne pas le moins du monde qu’un Rembrandt, un Beethoven, un Dante ou un Napoléon ont jamais existé ?'
var gagnant ={"mots":[],"size":0}

func _init():
	var myregex = RegEx.new()
	myregex.compile("\\w+")

	var idx = 0 
	while myregex.find(TEXT, idx) >= 0:
		var word = myregex.get_capture(0)
		var uniqueLetter = []
		for letter in word:
			if uniqueLetter.find(letter.to_lower()) == -1:
				uniqueLetter.append(letter.to_lower())
		if uniqueLetter.size() > gagnant.size:
			gagnant.mots.clear()
			gagnant.mots.append(word)
			gagnant.size = uniqueLetter.size()
		elif uniqueLetter.size() == gagnant.size:
			gagnant.mots.append(word)
		idx = myregex.get_capture_start(0) + word.length() 
	
	print(gagnant)
	quit()