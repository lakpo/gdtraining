extends Node2D

export var taille = Vector2(400,400)

export(int) var intervalX = 10 
export(int) var intervalY = 10 

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	set_fixed_process(true)

func _fixed_process(delta):
	update()
	
func _draw():
	var startX = 0 - taille[0]/2
	var startY = 0 - taille[1]/2

	for i in range(intervalX+1):
		
		var offsetX = taille[0] / intervalX * i
		draw_line(Vector2(startX + offsetX,startY),Vector2(startX + offsetX,startY + taille[1]),Color(1.0, 1.0, 1.0),4)
	for i in range(intervalY+1):
		
		var offsetY = taille[1] / intervalY * i
		draw_line(Vector2(startX,startY + offsetY),Vector2(startX + taille[0],startY + offsetY),Color(1.0, 1.0, 1.0),4)