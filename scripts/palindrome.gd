extends Node

const WORDS = ['mon', 'nom', 'possible', 'romuald', 'laumor', 'pierre', 'erreip' , 'ici' ,'ici' ]

func _ready():
	var pal = palindrome(WORDS)
	print(pal)

func strToArray(chaine):
	var tab = []
	for i in range(chaine.length()):
		tab.append(chaine.substr(i,1))
		
	return tab;

func palindrome(Arr):
	var palin = []
	var i = 0 
	for mot in Arr:
		var j = 0
		for mot2 in Arr:
			if i != j:
				var tab = strToArray(mot.to_lower() + mot2.to_lower())
				var tab2 = tab + []
				tab2.invert()
				if tab == tab2:
					palin.append(mot+mot2)
			j+= 1
		i +=1
	return palin